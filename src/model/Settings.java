/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.Preferences;

/**
 *
 * @author AndreyATC
 */
public class Settings {

    private static Preferences qSClientPrefs;
    private static String webServiceUrl = "/QueueSystem-war/PlasmaWS?wsdl";
    private static String shortWebServiceUrl;
    private File[] files;
    private int fileLength;
    private Double runningStringSpeed;
    private String runningStringFont;
    private Integer runningStringSize;
    private String runningString;
    private String headerString;
    private String headerStringFont;
    private Integer headerStringSize;
    private String textColor;
    private String backgroundColor;
    private String gridColNameFont;
    private Integer gridColNameSize;
    private String gridContentFont;
    private Integer gridContentSize;

    public Settings() {
        this.qSClientPrefs = Preferences.userRoot().node("QSPlasma");
        this.shortWebServiceUrl = qSClientPrefs.get("wsdlUrl", "http://192.168.19.200:8080");
    }

    public static URL getFullWebServiceUrl() {
        try {
            return new URL(shortWebServiceUrl + webServiceUrl);
        } catch (MalformedURLException ex) {
            Logger.getLogger(Settings.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public static String getShortWebServiceUrl() {
        return shortWebServiceUrl;
    }

    public static void setShortWebServiceUrl(String shortWebServiceUrl1) {
        shortWebServiceUrl = shortWebServiceUrl1;
        qSClientPrefs.put("wsdlUrl", shortWebServiceUrl);
    }
//------------------------------------------------------------------------------

    public void setFiles(List<File> files) {
        for (int i = 0; i < getFileLength(); i++) {
            qSClientPrefs.remove("media" + i);
        }
        int count = 0;
        for (File file : files) {
            String path = file.toURI().toString();
            qSClientPrefs.put("media" + count, path);
            count++;
        }
    }

    public LinkedList<String> getFiles() {
        fileLength = getFileLength();
        LinkedList<String> supplierNames = new LinkedList<>();
        files = new File[fileLength];
        for (int i = 0; i < fileLength;) {
            //String filePath = "file:/" + qSClientPrefs.get("media" + i, "").replace("\\", "/");
            String filePath = qSClientPrefs.get("media" + i, "");
            supplierNames.add(filePath);
            i++;
        }
        return supplierNames;
    }

    public void setFileLength(List<File> files) {
        int countOfFiles = 0;
        for (File file : files) {
            countOfFiles++;
        }
        this.fileLength = countOfFiles;
        qSClientPrefs.putInt("fileLength", fileLength);
    }

    public int getFileLength() {
        fileLength = qSClientPrefs.getInt("fileLength", 0);
        return fileLength;
    }

    public Double getRunningStringSpeed() {
        runningStringSpeed = qSClientPrefs.getDouble("RunningStringSpeed", 5.0);
        return runningStringSpeed;
    }

    public void setRunningStringSpeed(Double runningStringSpeed) {
        qSClientPrefs.putDouble("RunningStringSpeed", runningStringSpeed);
        this.runningStringSpeed = runningStringSpeed;
    }

    public String getRunningString() {
        runningString = qSClientPrefs.get("RunningString", "");
        return runningString;
    }

    public void setRunningString(String runningString) {
        qSClientPrefs.put("RunningString", runningString);
        this.runningString = runningString;
    }

    public String getHeaderString() {
        headerString = qSClientPrefs.get("HeaderString", "");
        return headerString;
    }

    public void setHeaderString(String headerString) {
        qSClientPrefs.put("HeaderString", headerString);
        this.headerString = headerString;
    }

    void getFiles(List<File> files) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public String getRunningStringFont() {
        runningStringFont = qSClientPrefs.get("RunningStringFont", "Arial");
        return runningStringFont;
    }

    public void setRunningStringFont(String runningStringFont) {
        qSClientPrefs.put("RunningStringFont", runningStringFont);
        this.runningStringFont = runningStringFont;
    }

    public Integer getRunningStringSize() {
        runningStringSize = qSClientPrefs.getInt("RunningStringSize", 18);
        return runningStringSize;
    }

    public void setRunningStringSize(Integer runningStringSize) {
        qSClientPrefs.putInt("RunningStringSize", runningStringSize);
        this.runningStringSize = runningStringSize;
    }

    public String getHeaderStringFont() {
        headerStringFont = qSClientPrefs.get("HeaderStringFont", "Arial");
        return headerStringFont;
    }

    public void setHeaderStringFont(String headerStringFont) {
        qSClientPrefs.put("HeaderStringFont", headerStringFont);
        this.headerStringFont = headerStringFont;
    }

    public Integer getHeaderStringSize() {
        headerStringSize = qSClientPrefs.getInt("HeaderStringSize", 18);
        return headerStringSize;
    }

    public void setHeaderStringSize(Integer headerStringSize) {
        qSClientPrefs.putInt("HeaderStringSize", headerStringSize);
        this.headerStringSize = headerStringSize;
    }

    public String getTextColor() {
        textColor = qSClientPrefs.get("TextColor", "Black");
        return textColor;
    }

    public void setTextColor(String textColor) {
        qSClientPrefs.put("TextColor", textColor);
        this.textColor = textColor;
    }

    public String getBackgroundColor() {
        backgroundColor = qSClientPrefs.get("BackgroundColor", "White");
        return backgroundColor;
    }

    public void setBackgroundColor(String backgroundColor) {
        qSClientPrefs.put("BackgroundColor", backgroundColor);
        this.backgroundColor = backgroundColor;
    }

    public String getGridContentFont() {
        gridContentFont = qSClientPrefs.get("GridContentFont", "Arial");
        return gridContentFont;
    }

    public void setGridContentFont(String gridContentFont) {
        qSClientPrefs.put("GridContentFont", gridContentFont);
        this.gridContentFont = gridContentFont;
    }

    public Integer getGridContentSize() {
        gridContentSize = qSClientPrefs.getInt("GridContentSize", 18);
        return gridContentSize;
    }

    public void setGridContentSize(Integer gridContentSize) {
        qSClientPrefs.putInt("GridContentSize", gridContentSize);
        this.gridContentSize = gridContentSize;
    }

    public String getGridColNameFont() {
        gridColNameFont = qSClientPrefs.get("GridColNameFont", "Arial");
        return gridColNameFont;
    }

    public void setGridColNameFont(String gridColNameFont) {
        qSClientPrefs.put("GridColNameFont", gridColNameFont);
        this.gridColNameFont = gridColNameFont;
    }

    public Integer getGridColNameSize() {
        gridColNameSize = qSClientPrefs.getInt("GridColNameSize", 18);
        return gridColNameSize;
    }

    public void setGridColNameSize(Integer gridColNameSize) {
        qSClientPrefs.putInt("GridColNameSize", gridColNameSize);
        this.gridColNameSize = gridColNameSize;
    }

}
