/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import javafx.animation.Timeline;
import javafx.animation.TranslateTransition;
import javafx.animation.TranslateTransitionBuilder;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.util.Duration;

/**
 *
 * @author Mudrik
 */
public class RunningString extends Pane {

    private String text;
    private Double speed;
    private Label labelText;
    private TranslateTransition translateTransition;

    public RunningString(String text, Double speed) {
        this.labelText = new Label(text);
        // this.text = text;
        this.speed = 600 / (10 * speed);
        this.getChildren().add(labelText);
    }

    public void run() {
        this.translateTransition = createAnimationRun(speed);
        translateTransition.play();
        labelText.setLayoutY(this.getHeight() / 2 - (labelText.prefHeight(-1) / 2));
    }

    public void stop() {
        if (translateTransition != null) {
            translateTransition.stop();
        }
    }

    public void setText(String text) {
        this.getChildren().remove(labelText);
        this.text = text;
        this.labelText = new Label(text);
        this.getChildren().add(labelText);
        labelText.impl_processCSS(true);
    }

    public void setTextFont(String font, Integer size) {
        labelText.setFont(Font.font(font, size));
    }

    public void update() {
        translateTransition.stop();
        translateTransition.play();
    }

    public void setSpeed(Double speed) {
        if (speed != null) {
            this.speed = 600 / (10 * speed);
        } else {
            this.speed = 600 / (10 * 1.0);
        }
        this.translateTransition = createAnimationRun(speed);
    }

    public void changeColor(String color) {
        labelText.setTextFill(Color.web(color));
    }

    public TranslateTransition createAnimationRun(Double speed) {
        TranslateTransition translate = TranslateTransitionBuilder.create()
                .duration(Duration.seconds(speed))
                .node(labelText)
                .fromX(getWidth() + labelText.prefWidth(-1))
                .toX(0 - labelText.prefWidth(-1))
                .cycleCount(Timeline.INDEFINITE)
                .autoReverse(false)
                .build();
        return translate;
    }
}
