/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import java.util.ResourceBundle;
import java.util.TimerTask;
import javafx.animation.FadeTransition;
import javafx.animation.FadeTransitionBuilder;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Duration;
import javax.swing.Timer;
import javax.xml.ws.BindingProvider;
import tray.FileChooseAction;
import tray.SettingDlgAction;
import tray.Tray;
import ua.com.alfametric.queuesystem.webservice.CurrentTask;

public class MyController implements Initializable, FileChooseAction, SettingDlgAction {

    @FXML
    private AnchorPane footerAnchor;
    @FXML
    private StackPane mediaPane;
    @FXML
    private AnchorPane root1;
    @FXML
    private Label headerText;
    @FXML
    private Label talonNumber;
    @FXML
    private Label place;
    @FXML
    private AnchorPane headerAnchor;
    @FXML
    private GridPane mainGrid;
    @FXML
    private GridPane dataGrid;
    @FXML
    private Label LabelLine1Check;
    @FXML
    private Label LabelLine2Check;
    @FXML
    private Label LabelLine3Check;
    @FXML
    private Label LabelLine4Check;
    @FXML
    private Label LabelLine5Check;
    @FXML
    private Label LabelLine1Place;
    @FXML
    private Label LabelLine2Place;
    @FXML
    private Label LabelLine3Place;
    @FXML
    private Label LabelLine4Place;
    @FXML
    private Label LabelLine5Place;

    Stage stage;

    private Settings settings;
    private RunningString runStr;
    private MediaContentPlayer mediaContentPlayer;
    private Timer pingTimer;
    private long currentTask1, currentTask2, currentTask3, currentTask4, currentTask5;
    private int currentTaskId1, currentTaskId2, currentTaskId3, currentTaskId4, currentTaskId5 = 0;
    private FadeTransition fadeTransition;
    private List currentTasksId = new ArrayList();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        System.out.println("model.Controller.initialize()");
        settings = new Settings();
        createRunningStr();
        createMedia();
        setHeaderText(settings.getHeaderString());
        setHeaderTextFont(settings.getHeaderStringFont(), settings.getRunningStringSize());
        setGridColNameFont(settings.getGridColNameFont(), settings.getGridColNameSize());
        setGridContentFont(settings.getGridContentFont(), settings.getGridContentSize());
        Tray tray = new Tray();
        tray.setFileChooseActionListener(this);
        tray.setSettingDlgAction(this);
        pingTimer();
    }

    public void createRunningStr() {
        runStr = new RunningString("TExt", 2.0);
        AnchorPane.setLeftAnchor(runStr, 0.0);
        AnchorPane.setRightAnchor(runStr, 0.0);
        AnchorPane.setTopAnchor(runStr, 0.0);
        AnchorPane.setBottomAnchor(runStr, 0.0);
        footerAnchor.getChildren().add(runStr);
        runStr.setText(settings.getRunningString());
        runStr.setTextFont(settings.getRunningStringFont(),
                settings.getRunningStringSize());
    }

    public void createMedia() {
        try {
            mediaContentPlayer = new MediaContentPlayer();
            mediaContentPlayer.setListMediaPath(settings.getFiles());
            mediaContentPlayer.playFirstMedia();
            mediaPane.getChildren().add(mediaContentPlayer);
            StackPane.setAlignment(mediaContentPlayer, Pos.BOTTOM_CENTER);
        } catch (Exception e) {
        }
    }

    public void changeMedia() {
        mediaContentPlayer.setListMediaPath(settings.getFiles());
        mediaContentPlayer.playFirstMedia();
    }

    public void mediaWidth(double newValue) {
        try {
            mediaContentPlayer.setMediaWidth((double) newValue / 2);
        } catch (Exception e) {
        }
    }

    public void starRunningString() {
        runStr.run();
    }

    public void setRunningStrText(String text) {
        runStr.setText(text);
    }

    public void setRunningStrSpeed(Double speed) {
        runStr.setSpeed(speed);
    }

    public void setRunningStrFont(String font, Integer size) {
        runStr.setTextFont(font, size);
    }

    public void setHeaderText(String headerText) {
        this.headerText.setText(headerText);
    }

    public void setHeaderTextFont(String font, Integer size) {
        this.headerText.setFont(Font.font(font, size));
    }

    public void setBackgroundColor(Color color) {
        String qwe = String.format("#%02X%02X%02X",
                (int) (color.getRed() * 255),
                (int) (color.getGreen() * 255),
                (int) (color.getBlue() * 255));
        root1.setBackground(new Background(new BackgroundFill(color, CornerRadii.EMPTY, Insets.EMPTY)));
    }

    public void setTextColor(Color color) {
        String qwe = String.format("#%02X%02X%02X",
                (int) (color.getRed() * 255),
                (int) (color.getGreen() * 255),
                (int) (color.getBlue() * 255));
        String colorStyle = "-fx-border-width: 5px ; -fx-border-color:" + qwe;
        mainGrid.setStyle(colorStyle);
        dataGrid.setStyle(colorStyle);
        runStr.changeColor(qwe);
        headerText.setTextFill(Color.web(qwe));
        talonNumber.setTextFill(Color.web(qwe));
        place.setTextFill(Color.web(qwe));
        LabelLine1Check.setTextFill(Color.web(qwe));
        LabelLine2Check.setTextFill(Color.web(qwe));
        LabelLine3Check.setTextFill(Color.web(qwe));
        LabelLine4Check.setTextFill(Color.web(qwe));
        LabelLine5Check.setTextFill(Color.web(qwe));
        LabelLine1Place.setTextFill(Color.web(qwe));
        LabelLine2Place.setTextFill(Color.web(qwe));
        LabelLine3Place.setTextFill(Color.web(qwe));
        LabelLine4Place.setTextFill(Color.web(qwe));
        LabelLine5Place.setTextFill(Color.web(qwe));
    }

    public void setGridColNameFont(String font, Integer size) {
        talonNumber.setFont(Font.font(font, size));
        place.setFont(Font.font(font, size));
    }

    public void setGridContentFont(String font, Integer size) {
        LabelLine1Check.setFont(Font.font(font, size));
        LabelLine2Check.setFont(Font.font(font, size));
        LabelLine3Check.setFont(Font.font(font, size));
        LabelLine4Check.setFont(Font.font(font, size));
        LabelLine5Check.setFont(Font.font(font, size));
        LabelLine1Place.setFont(Font.font(font, size));
        LabelLine2Place.setFont(Font.font(font, size));
        LabelLine3Place.setFont(Font.font(font, size));
        LabelLine4Place.setFont(Font.font(font, size));
        LabelLine5Place.setFont(Font.font(font, size));
    }

    public void setHeaderAndFooterSize(double height) {
        footerAnchor.setPrefHeight(height / 100 * 15);
        headerAnchor.setPrefHeight(height / 100 * 15);
    }

    public void eventOnLoadMainPage() {
        setBackgroundColor(Color.valueOf(settings.getBackgroundColor()));
        setTextColor(Color.valueOf(settings.getTextColor()));
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @Override
    public void OnShowFileChooser() {
        Task task = new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                Platform.runLater(() -> {
                    List<File> files = null;
                    FileChooser fileChooser = new FileChooser();
                    FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("ALL files (*.*)", "*.*");
                    fileChooser.getExtensionFilters().add(extFilter);
                    try {
                        files = fileChooser.showOpenMultipleDialog(stage);
                        String[] filePathArray = new String[files.size()];
                        int i = 0;
                        for (int j = 0; j < files.size(); j++) {
                            filePathArray[j] = files.get(j).toURI().toString();
                        }
                        mediaContentPlayer.setListMediaPath(filePathArray);
                        mediaContentPlayer.playFirstMedia();
                        if (mediaContentPlayer.isMediaFileIsCorrect()) {
                            settings.setFiles(files);
                            settings.setFileLength(files);
                        }
                    } catch (Exception e) {
                        System.out.println("жодного файлу не вибрано");
                    }
                });
                return null;
            }
        };
        Thread settingsStageThread = new Thread(task);
        settingsStageThread.start();
    }

    @Override
    public void createSettingDlgAction() {
        Task task = new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                Platform.runLater(() -> {
                    createSettingWindow();
                });
                return null;
            }
        };
        Thread settingsStageThread = new Thread(task);
        settingsStageThread.start();
    }

    public void createSettingWindow() {
        Stage settingStage = new Stage();
        ObservableList<Double> speedStrList = FXCollections.observableArrayList(1.0, 2.0, 3.0, 4.0, 5.0);
        ObservableList<String> fonts = FXCollections.observableArrayList("Arial", "Aharoni", "Arial Black", "Calibri", "Calibri Light", "Tahoma");
        ObservableList<Integer> fontsSize = FXCollections.observableArrayList(12, 14, 16, 20, 24, 32, 36, 40, 48);

        //контейнери
        AnchorPane mainPane = new AnchorPane();
        BorderPane borderPane = new BorderPane();
        VBox leftVBox = new VBox();
        VBox rightVBox = new VBox();
        HBox hBoxRunStrInputText = new HBox();
        HBox hBoxRunStrFontAndSize = new HBox();
        HBox hBoxHeaderFontAndSize = new HBox();
        HBox hBoxGritColumNameFontAndSize = new HBox();
        HBox hBoxGritContentNameFontAndSize = new HBox();
        HBox hBoxButtons = new HBox();

        //лейбли
        Label labelUrl = new Label();
        labelUrl.setText("Адреса сервера");
        Label labelText = new Label();
        labelText.setText("Біжучий рядок");
        Label labelTextFont = new Label();
        labelTextFont.setText("Параметри біжучого рядка");
        Label labelHeaderText = new Label();
        labelHeaderText.setText("Заголовок");
        Label labelHeaderTextFont = new Label();
        labelHeaderTextFont.setText("Параметри заголовку");
        Label labelGritColumNameFont = new Label();
        labelGritColumNameFont.setText("Параметри назв колонок");
        Label labelGritContentNameFont = new Label();
        labelGritContentNameFont.setText("Параметри данних таблиці");
        Label labelBackgroundAndTextColor = new Label();
        labelBackgroundAndTextColor.setText("Колір фону та тексту");

        //поля заповнення/вибору
        TextField textFieldUrl = new TextField();
        textFieldUrl.setPrefWidth(255);
        TextField textFieldText = new TextField();
        textFieldText.setPrefWidth(200);
        TextField textFieldHeaderText = new TextField();
        textFieldHeaderText.setPrefWidth(255);
        ComboBox<Double> comboBoxTextSpeed = new ComboBox<>();
        ComboBox<String> comboBoxTextFont = new ComboBox<>();
        ComboBox<Integer> comboBoxTextSize = new ComboBox<>();
        ComboBox<String> comboBoxHeaderTextFont = new ComboBox<>();
        ComboBox<Integer> comboBoxHeaderTextSize = new ComboBox<>();
        ComboBox<String> comboBoxGritColumNameFont = new ComboBox<>();
        ComboBox<Integer> comboBoxGritColumNameSize = new ComboBox<>();
        ComboBox<String> comboBoxGritContentNameFont = new ComboBox<>();
        ComboBox<Integer> comboBoxGritContentNameSize = new ComboBox<>();
        ColorPicker colorPickerBackground = new ColorPicker();
        ColorPicker colorPickerText = new ColorPicker();

        //запис з регіструs
        textFieldUrl.setText(settings.getShortWebServiceUrl());
        textFieldText.setText(settings.getRunningString());
        textFieldHeaderText.setText(settings.getHeaderString());

        comboBoxTextSpeed.setItems(speedStrList);
        comboBoxTextSpeed.setValue(settings.getRunningStringSpeed());
        comboBoxTextFont.setItems(fonts);
        comboBoxTextFont.setValue(settings.getRunningStringFont());
        comboBoxTextSize.setItems(fontsSize);
        comboBoxTextSize.setValue(settings.getRunningStringSize());

        comboBoxHeaderTextFont.setItems(fonts);
        comboBoxHeaderTextFont.setValue(settings.getHeaderStringFont());
        comboBoxHeaderTextSize.setItems(fontsSize);
        comboBoxHeaderTextSize.setValue(settings.getHeaderStringSize());

        comboBoxGritColumNameFont.setItems(fonts);
        comboBoxGritColumNameFont.setValue(settings.getGridColNameFont());
        comboBoxGritColumNameSize.setItems(fontsSize);
        comboBoxGritColumNameSize.setValue(settings.getGridColNameSize());

        comboBoxGritContentNameFont.setItems(fonts);
        comboBoxGritContentNameFont.setValue(settings.getGridContentFont());
        comboBoxGritContentNameSize.setItems(fontsSize);
        comboBoxGritContentNameSize.setValue(settings.getGridContentSize());

        //color ---------------------------------------------
        colorPickerBackground.setValue(Color.valueOf(settings.getBackgroundColor()));
        colorPickerText.setValue(Color.valueOf(settings.getTextColor()));
        HBox hBoxColors = new HBox();
        hBoxColors.getChildren().addAll(colorPickerBackground, colorPickerText);

        //кнопки
        Button submit = new Button();
        submit.setText("Підтвердити");
        Button cancel = new Button();
        cancel.setText("Відміна");
//-----------------------------------------------------------------------------------------------------------

//-----------------------------------------------------------------------------------------------------------
        hBoxRunStrInputText.getChildren().addAll(textFieldText, comboBoxTextSpeed);
        hBoxRunStrFontAndSize.getChildren().addAll(comboBoxTextFont, comboBoxTextSize);
        hBoxHeaderFontAndSize.getChildren().addAll(comboBoxHeaderTextFont, comboBoxHeaderTextSize);
        hBoxGritColumNameFontAndSize.getChildren().addAll(comboBoxGritColumNameFont, comboBoxGritColumNameSize);
        hBoxGritContentNameFontAndSize.getChildren().addAll(comboBoxGritContentNameFont, comboBoxGritContentNameSize);
        hBoxButtons.getChildren().addAll(submit, cancel);

        hBoxRunStrInputText.setSpacing(5);
        hBoxRunStrFontAndSize.setSpacing(5);
        hBoxHeaderFontAndSize.setSpacing(5);
        hBoxGritColumNameFontAndSize.setSpacing(5);
        hBoxGritContentNameFontAndSize.setSpacing(5);
        hBoxColors.setSpacing(5);
        hBoxButtons.setSpacing(5);

        leftVBox.getChildren().addAll(labelUrl, labelText, labelTextFont,
                labelHeaderText, labelHeaderTextFont, labelGritColumNameFont,
                labelGritContentNameFont, labelBackgroundAndTextColor);

        rightVBox.getChildren().addAll(textFieldUrl, hBoxRunStrInputText,
                hBoxRunStrFontAndSize, textFieldHeaderText,
                hBoxHeaderFontAndSize, hBoxGritColumNameFontAndSize,
                hBoxGritContentNameFontAndSize, hBoxColors, hBoxButtons);

        hBoxButtons.setAlignment(Pos.CENTER_RIGHT);
        hBoxRunStrInputText.setAlignment(Pos.CENTER_RIGHT);
        hBoxRunStrFontAndSize.setAlignment(Pos.CENTER_RIGHT);
        hBoxHeaderFontAndSize.setAlignment(Pos.CENTER_RIGHT);
        hBoxGritColumNameFontAndSize.setAlignment(Pos.CENTER_RIGHT);
        hBoxGritContentNameFontAndSize.setAlignment(Pos.CENTER_RIGHT);
        hBoxColors.setAlignment(Pos.CENTER_RIGHT);

        VBox.setMargin(hBoxButtons, new Insets(30, 0, 0, 30));
        leftVBox.setPadding(new Insets(5, 20, 0, 10));
        leftVBox.setSpacing(11);
        rightVBox.setPadding(new Insets(3, 10, 0, 20));
        rightVBox.setSpacing(3);
        borderPane.setLeft(leftVBox);
        borderPane.setRight(rightVBox);

        mainPane.getChildren().add(borderPane);

        //обробка кнопок
        //confirm
        submit.setOnAction((ActionEvent e) -> {
            settings.setRunningString(textFieldText.getText());
            settings.setShortWebServiceUrl(textFieldUrl.getText());
            settings.setHeaderString(textFieldHeaderText.getText());
            if (comboBoxTextSpeed.getValue() != null) {
                settings.setRunningStringSpeed(comboBoxTextSpeed.getValue());
            }
            if (comboBoxTextFont.getValue() != null) {
                settings.setRunningStringFont(comboBoxTextFont.getValue());
            }
            if (comboBoxTextSize.getValue() != null) {
                settings.setRunningStringSize(comboBoxTextSize.getValue());
            }
            if (comboBoxHeaderTextFont.getValue() != null) {
                settings.setHeaderStringFont(comboBoxHeaderTextFont.getValue());
            }
            if (comboBoxHeaderTextSize.getValue() != null) {
                settings.setHeaderStringSize(comboBoxHeaderTextSize.getValue());
            }
            if (comboBoxGritColumNameFont.getValue() != null) {
                settings.setGridColNameFont(comboBoxGritColumNameFont.getValue());
            }
            if (comboBoxGritColumNameSize.getValue() != null) {
                settings.setGridColNameSize(comboBoxGritColumNameSize.getValue());
            }
            if (comboBoxGritContentNameFont.getValue() != null) {
                settings.setGridContentFont(comboBoxGritContentNameFont.getValue());
            }
            if (comboBoxGritContentNameSize.getValue() != null) {
                settings.setGridContentSize(comboBoxGritContentNameSize.getValue());
            }

            //color p2 --------------------------------------------------------------
            settings.setBackgroundColor(colorPickerBackground.getValue().toString());
            settings.setTextColor(colorPickerText.getValue().toString());

            setRunningStrText(textFieldText.getText());
            setRunningStrSpeed(comboBoxTextSpeed.getValue());
            setRunningStrFont(comboBoxTextFont.getValue(), comboBoxTextSize.getValue());
            starRunningString();
            setGridColNameFont(comboBoxGritColumNameFont.getValue(), comboBoxGritColumNameSize.getValue());
            setGridContentFont(comboBoxGritContentNameFont.getValue(), comboBoxGritContentNameSize.getValue());
            setHeaderText(textFieldHeaderText.getText());
            setHeaderTextFont(comboBoxHeaderTextFont.getValue(), comboBoxHeaderTextSize.getValue());

            setBackgroundColor(colorPickerBackground.getValue());
            setTextColor(colorPickerText.getValue());

            settingStage.close();
        });
        //cancel
        cancel.setOnAction((ActionEvent e) -> {

            settingStage.close();
        });
        settingStage.setTitle("Setting");
        Scene scene = new Scene(mainPane);
        settingStage.setScene(scene);
        settingStage.setResizable(false);
        settingStage.show();
    }

//-------------------------------------------------------------------------------------------------------------------------
    private void pingTimer() {
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                refreshPlasmaStatus();
            }
        };
        java.util.Timer timer = new java.util.Timer("testTimer");
        timer.schedule(timerTask, 1000, 10000);
    }

    public void refreshPlasmaStatus() {
        long dateTime = new Date().getTime();

        java.util.List<CurrentTask> listCurrentTaskForPlasma = new ArrayList<CurrentTask>();
        try {
            listCurrentTaskForPlasma = getListCurrentTaskForPlasma();
        } catch (Exception e) {
        }
        Platform.runLater(() -> {
            LabelLine1Check.setText("---");
            LabelLine2Check.setText("---");
            LabelLine3Check.setText("---");
            LabelLine4Check.setText("---");
            LabelLine5Check.setText("---");
            LabelLine1Place.setText("---");
            LabelLine2Place.setText("---");
            LabelLine3Place.setText("---");
            LabelLine4Place.setText("---");
            LabelLine5Place.setText("---");
        });

        currentTask1 = 0;
        currentTask2 = 0;
        currentTask3 = 0;
        currentTask4 = 0;
        currentTask5 = 0;

        for (int i = 0; i < listCurrentTaskForPlasma.size(); i++) {
            CurrentTask currentTask = listCurrentTaskForPlasma.get(i);
            switch (i) {
                case 0: {
                    Platform.runLater(() -> {
                        LabelLine5Check.setText(currentTask.getCheckNumber().toString());
                        LabelLine5Place.setText(currentTask.getPersonId().getLastWorkplaceNumber().toString());
                    });
                    if (currentTask.getId() != currentTaskId5) {
                        currentTasksId.remove(currentTask5);
                        currentTasksId.add(currentTask.getId());
                        currentTaskId5 = currentTask.getId();
                        currentTask5 = new Date().getTime();
                        System.out.println("model.MyController.refreshPlasmaStatus()");
                    } else {
                        
                    }

                    if (dateTime - currentTask5 <= 10000) {
                        FadeTransition fadeTransitionLabelLine5Check = blinkAnimation(LabelLine5Check);
                        FadeTransition fadeTransitionLabelLine5Place = blinkAnimation(LabelLine5Place);
                        fadeTransitionLabelLine5Check.play();
                        fadeTransitionLabelLine5Place.play();
                    }
                    continue;
                }
                case 1: {
                    Platform.runLater(() -> {
                        LabelLine4Check.setText(currentTask.getCheckNumber().toString());
                        LabelLine4Place.setText(currentTask.getPersonId().getLastWorkplaceNumber().toString());
                    });
                    currentTasksId.add(currentTask.getId());
                    currentTask4 = currentTask.getCallDatetime().toGregorianCalendar().getTimeInMillis();
                    if (currentTask.getId() != currentTaskId4) {
                        currentTasksId.remove(currentTask4);
                        currentTasksId.add(currentTask.getId());
                        currentTaskId4 = currentTask.getId();
                        currentTask4 = new Date().getTime();
                        System.out.println("model.MyController.refreshPlasmaStatus()");
                    } else {
                    }
                    if (dateTime - currentTask4 <= 10000) {
                        FadeTransition fadeTransitionLabelLine4Check = blinkAnimation(LabelLine4Check);
                        FadeTransition fadeTransitionLabelLine4Place = blinkAnimation(LabelLine4Place);
                        fadeTransitionLabelLine4Check.play();
                        fadeTransitionLabelLine4Place.play();
                    }
                    continue;
                }
                case 2: {
                    Platform.runLater(() -> {
                        LabelLine3Check.setText(currentTask.getCheckNumber().toString());
                        LabelLine3Place.setText(currentTask.getPersonId().getLastWorkplaceNumber().toString());
                    });
                    currentTask3 = currentTask.getCallDatetime().toGregorianCalendar().getTimeInMillis();
                    if (dateTime - currentTask3 <= 10000) {
                        FadeTransition fadeTransitionLabelLine3Check = blinkAnimation(LabelLine3Check);
                        FadeTransition fadeTransitionLabelLine3Place = blinkAnimation(LabelLine3Place);
                        fadeTransitionLabelLine3Check.play();
                        fadeTransitionLabelLine3Place.play();
                    }
                    continue;
                }
                case 3: {
                    Platform.runLater(() -> {
                        LabelLine2Check.setText(currentTask.getCheckNumber().toString());
                        LabelLine2Place.setText(currentTask.getPersonId().getLastWorkplaceNumber().toString());
                    });
                    currentTask2 = currentTask.getCallDatetime().toGregorianCalendar().getTimeInMillis();
                    if (dateTime - currentTask2 <= 10000) {
                        FadeTransition fadeTransitionLabelLine2Check = blinkAnimation(LabelLine2Check);
                        FadeTransition fadeTransitionLabelLine2Place = blinkAnimation(LabelLine2Place);
                        fadeTransitionLabelLine2Check.play();
                        fadeTransitionLabelLine2Place.play();
                    }
                    continue;
                }
                case 4: {
                    Platform.runLater(() -> {
                        LabelLine1Check.setText(currentTask.getCheckNumber().toString());
                        LabelLine1Place.setText(currentTask.getPersonId().getLastWorkplaceNumber().toString());
                    });
                    currentTask1 = currentTask.getCallDatetime().toGregorianCalendar().getTimeInMillis();
                    if (dateTime - currentTask5 <= 10000) {
                        FadeTransition fadeTransitionLabelLine1Check = blinkAnimation(LabelLine1Check);
                        FadeTransition fadeTransitionLabelLine1Place = blinkAnimation(LabelLine1Place);
                        fadeTransitionLabelLine1Check.play();
                        fadeTransitionLabelLine1Place.play();
                    }
                    continue;
                }
            }
        }
    }

//end
    private static java.util.List<ua.com.alfametric.queuesystem.webservice.CurrentTask> getListCurrentTaskForPlasma() {
        ua.com.alfametric.queuesystem.webservice.PlasmaWS_Service service = new ua.com.alfametric.queuesystem.webservice.PlasmaWS_Service(Settings.getFullWebServiceUrl());
        ua.com.alfametric.queuesystem.webservice.PlasmaWS port = service.getPlasmaWSPort();
        ((BindingProvider) port).getRequestContext().put(
                "ua.com.alfametric.queuesystem.plasma.webservice", 2000);
        return port.getListCurrentTaskForPlasma();
    }

    public FadeTransition blinkAnimation(Label label) {
        fadeTransition = FadeTransitionBuilder.create()
                .duration(Duration.seconds(0.5))
                .node(label)
                .fromValue(1)
                .toValue(0.2)
                .cycleCount(16)
                .autoReverse(true)
                .build();
        return fadeTransition;
    }
//--------------------------------------------------------------------------------------------------------------------------
}
