/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.FadeTransition;
import javafx.animation.FadeTransitionBuilder;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.StackPane;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.util.Duration;

/**
 *
 * @author Andre
 */
public final class MediaContentPlayer extends StackPane {

    private int currentPathIndex;
    private boolean endMediaTransStarted;
    public boolean playerEnabled;
    private MediaPlayer mPlayer;
    private final MediaView mediaView;
    private Media media;
    private LinkedList<String> listMediaPath;
    private boolean mediaFileIsCorrect = false;

    public MediaContentPlayer() {
        this.mediaView = new MediaView();
        this.listMediaPath = new LinkedList<>();
        initMediaContentPlayer();
    }

    public MediaContentPlayer(double width, double height) {
        this.mediaView = new MediaView();
        this.listMediaPath = new LinkedList<>();
        setMediaHeight(height);
        setMediaWidth(width);
        initMediaContentPlayer();
    }

    private void initMediaContentPlayer() {
        getChildren().add(mediaView);
        currentPathIndex = 0;
    }

    public void setListMediaPath(LinkedList<String> listMediaPath) {
        this.listMediaPath = new LinkedList<>(listMediaPath);
    }

    public void setListMediaPath(String[] arrayMediaPath) {
        this.listMediaPath = new LinkedList<>(Arrays.asList(arrayMediaPath));
    }

    private void playMedia(String path) {
        closeMedia();
        try {
            try {
                media = new Media(path);
            } catch (Exception e) {
                mediaFileIsCorrect = false;
                if (path != "") {
                    Alert alert = new Alert(AlertType.INFORMATION);
                    alert.setTitle("Медіа файл");
                    alert.setHeaderText("Вибраний невірний формат файлу");
                    alert.setContentText("Формати, які підтримуються:\n"
                            + "Відео: FXM, FLV, MP4, HLS (*)\n"
                            + "Аудіо: MP3, WAV, AIFF , HLS (*)\n");
                    alert.showAndWait();
                } else {
                }
                return;
            }
            mediaFileIsCorrect = true;
            mPlayer = new MediaPlayer(media);
            mPlayer.currentTimeProperty().addListener((observableValue, oldDuration, newDuration) -> {
                if (newDuration.greaterThan(mPlayer.getStopTime().subtract(Duration.seconds(1))) && !endMediaTransStarted) {
                    endMediaTransStarted = true;
                    nodeFadeTransition(mediaView, 1, 1, 0);
                }
            });
            mPlayer.setOnEndOfMedia(() -> {
                playNextMedia();
                nodeFadeTransition(mediaView, 2, 0, 1);
            });
            mediaView.setMediaPlayer(mPlayer);
            mPlayer.play();
            endMediaTransStarted = false;
            playerEnabled = true;
        } catch (Exception e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
            playNextMedia();
        }
    }

    private void playNextMedia() {
        if (!listMediaPath.isEmpty()) {
            currentPathIndex = ++currentPathIndex >= listMediaPath.size() ? 0 : currentPathIndex;
            String path = listMediaPath.get(currentPathIndex);
            playMedia(path);
        }
    }

    public void playFirstMedia() {
        if (!listMediaPath.isEmpty()) {
            playMedia(listMediaPath.get(0));
        }
    }

    public void setMediaWidth(double width) {
        mediaView.setFitWidth(width);
    }

    public void setMediaHeight(double height) {
        mediaView.setFitHeight(height);
    }

    private void nodeFadeTransition(Node node, double dur, double from, double to) {
        FadeTransition fadeTransition = FadeTransitionBuilder.create()
                .duration(Duration.seconds(dur))
                .node(node)
                .fromValue(from)
                .toValue(to)
                .cycleCount(1)
                .autoReverse(false)
                .build();
        fadeTransition.play();
    }

    public void addMediaPath(String mediaPath) {
        listMediaPath.add(mediaPath);
    }

    public void closeMedia() {

        if (playerEnabled) {
            mPlayer.stop();
            mPlayer.dispose();
            System.gc();
            playerEnabled = false;
        }
    }

    public void removeMediaPath(String mediaPath) {
        listMediaPath.remove(mediaPath);
    }

    public boolean isMediaFileIsCorrect() {
        return mediaFileIsCorrect;
    }

}
