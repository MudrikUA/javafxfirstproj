/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.IOException;
import javafx.application.Application;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;

import javafx.stage.Stage;
import javafx.stage.WindowEvent;

/**
 *
 * @author Mudrik
 */
public class Main extends Application {

    //public MyController controller;
    private Settings settings;
    final KeyCombination keyCombinatoin = new KeyCodeCombination(KeyCode.ENTER,
            KeyCombination.ALT_DOWN);
    final KeyCombination escKey = new KeyCodeCombination(KeyCode.ESCAPE);

    @Override
    public void start(Stage primaryStage) throws IOException {
        primaryStage.setTitle("Plasma");
        FXMLLoader loader = new FXMLLoader();
        Parent root = (Parent) loader.load(MyController.class.getResourceAsStream("/view/plasma_1.fxml"));
        final MyController controller = (MyController) loader.getController();
        Scene scene = new Scene(root);
        primaryStage.setScene(scene);
        primaryStage.show();
        //events
        stageShowEvent(primaryStage, controller);
        stageCloseEvent(primaryStage);
        stageResizeEvent(primaryStage, controller);
        controller.setStage(primaryStage);
        stageShowEvent(primaryStage, controller);
        hotKeyAction(primaryStage, controller);
        controller.eventOnLoadMainPage();
        primaryStage.setFullScreen(true);
        controller.starRunningString();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    public void hotKeyAction(Stage primaryStage, MyController controller) {
        primaryStage.getScene().setOnKeyPressed(new EventHandler<KeyEvent>() {
            public void handle(KeyEvent keyEvent) {
                if (keyCombinatoin.match(keyEvent)) {
                    primaryStage.setFullScreen(true);
                    controller.starRunningString();
                }
                if (escKey.match(keyEvent)) {
                    controller.starRunningString();
                    controller.mediaWidth(primaryStage.getWidth()-20);
                }
            }
        });
    }

    public void stageShowEvent(Stage primaryStage, MyController controller) {
        primaryStage.addEventHandler(WindowEvent.WINDOW_SHOWN, (WindowEvent window) -> {
            controller.eventOnLoadMainPage();
        });
    }

    public void stageCloseEvent(Stage primaryStage) {
        primaryStage.addEventHandler(WindowEvent.WINDOW_CLOSE_REQUEST, (WindowEvent window) -> {
            System.exit(0);
        });
    }

    public void stageResizeEvent(Stage primaryStage, MyController controller) {
        primaryStage.widthProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            controller.starRunningString();
            if (primaryStage.isFullScreen()) {
                controller.mediaWidth((double) newValue);
            } else {
                controller.mediaWidth((double) newValue - 20);
            }

        });
        primaryStage.heightProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            controller.setHeaderAndFooterSize((double) newValue);
        });
    }

}
