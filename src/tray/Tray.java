/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tray;

import java.awt.Image;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.Toolkit;
import java.awt.TrayIcon;
import java.awt.event.ActionListener;
import java.net.URL;
import javax.swing.ImageIcon;

/**
 *
 * @author Mudrik
 */
public class Tray {

    private FileChooseAction fileChooseActionListener;
    private SettingDlgAction settingDlgAction;

    public Tray() {
        if (SystemTray.isSupported()) {
            SystemTray tray = SystemTray.getSystemTray();
            ImageIcon imageIcon = new javax.swing.ImageIcon(getClass().getResource("/res/tray.png"));
            PopupMenu popup = new PopupMenu();
            MenuItem item = new MenuItem("Exit");
            MenuItem settingsMenu = new MenuItem();
            MenuItem fileChooser = new MenuItem();
            MenuItem skipMediaFile = new MenuItem();

            fileChooser.setLabel("Вибір медіа файлу");
            fileChooser.addActionListener((java.awt.event.ActionEvent evt) -> {
                if (fileChooseActionListener != null) {
                    fileChooseActionListener.OnShowFileChooser();
                }
            });

            settingsMenu.setLabel("Налаштування");
            settingsMenu.addActionListener((java.awt.event.ActionEvent evt) -> {
                if (settingDlgAction != null) {
                    settingDlgAction.createSettingDlgAction();
                }
            });

            //poopup add
            popup.add(fileChooser);
            popup.add(settingsMenu);
            popup.add(item);
            //create tray and add popup to tray
            TrayIcon trayIcon = new TrayIcon(imageIcon.getImage(), "Plasma", popup);

            ActionListener listener = (java.awt.event.ActionEvent arg0) -> {
                // TODO Auto-generated method stub
                System.exit(0);
            };
            item.addActionListener(listener);
            try {
                tray.add(trayIcon);
            } catch (Exception e) {
                System.err.println("Can't add to tray");
            }
        } else {
            System.err.println("Tray unavailable");
        }
    }

    public void setFileChooseActionListener(FileChooseAction fileChooseActionListener) {
        this.fileChooseActionListener = fileChooseActionListener;
    }

    public void setSettingDlgAction(SettingDlgAction settingDlgAction) {
        this.settingDlgAction = settingDlgAction;
    }

}
